# Update packages in the repositories
apt update

# Install dependencies
apt install -y golang make bzip2

# Install MySQL
apt install -y mysql-server

# MySQL Post-installation
mysql_secure_installation

# User for MySQL Server Exporter
mysql -e "ALTER USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password BY 'root12345'; FLUSH PRIVILEGES"
mysql -u root -p -e "CREATE USER 'exporter'@'%' IDENTIFIED BY 'password' WITH MAX_USER_CONNECTIONS 3; GRANT PROCESS, REPLICATION CLIENT, SELECT ON *.* TO 'exporter'@'%';"

# Add Grafana repositories
apt install -y apt-transport-https software-properties-common wget
mkdir -p /etc/apt/keyrings/
wget -q -O - https://apt.grafana.com/gpg.key | gpg --dearmor | sudo tee /etc/apt/keyrings/grafana.gpg > /dev/null
echo "deb [signed-by=/etc/apt/keyrings/grafana.gpg] https://apt.grafana.com stable main" | sudo tee -a /etc/apt/sources.list.d/grafana.list

# Install Grafana
apt update
apt install grafana

# Start Grafana
systemctl enable grafana-server
systemctl start grafana-server

# Install NVM, Node.js and NPM
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.7/install.sh | bash
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion
nvm install 20

# Install MySQL Server Exporter
git clone https://github.com/prometheus/mysqld_exporter
cd mysqld_exporter
make build
cd ..

# Install Node exporter
git clone https://github.com/prometheus/node_exporter
cd node_exporter
make build
cd ..

# Install Alertmanager
git clone https://github.com/prometheus/alertmanager
cd alertmanager
make build
cd ..

# Install Prometheus
git clone https://github.com/prometheus/prometheus
cd prometheus
make build
cd ..

# Prometheus TLS Configuration - Self-signed certificate and private key
openssl req -x509 -newkey rsa:4096 -nodes -keyout prom.key -out prom.crt
chmod 644 prom.key

# Servers initialization
./mysqld_exporter/mysqld_exporter --config.my-cnf=config.my-cnf & && \
./node_exporter/node_exporter & && \
./alertmanager/alertmanager --config.file=alertmanager.yml & && \
./prometheus/prometheus --web.config.file=web.yml --config.file=prometheus.yml &
